package lib.captura.video.core.storage;


import lib.captura.video.core.InformationAssert;
import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.Message;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

class StorageProxyTest {

    @Test
    void whenPathNullShouldSendEvent() {
        assertRaw
                (
                        new Raw() {
                            @Override
                            public Metadata metadata() {
                                return new MetadataSkeleton();
                            }

                            @Override
                            public String videoFilePath() {
                                return null;
                            }
                        }
                );
    }

    @Test
    void whenNameIsBlankShouldSendEvent() {
        assertRaw
                (
                        new Raw() {

                            @Override
                            public Metadata metadata() {

                                return new MetadataSkeleton() {

                                    @Override
                                    public String name() {
                                        return "";
                                    }
                                };
                            }

                            @Override
                            public String videoFilePath() {
                                return "123";
                            }
                        }
                );
    }

    @Test
    void whenAuthorIsBlankShouldSendEvent() {
        assertRaw
                (
                        new Raw() {
                            @Override
                            public Metadata metadata() {

                                return new MetadataSkeleton() {

                                    @Override
                                    public String author() {
                                        return "";
                                    }

                                };
                            }

                            @Override
                            public String videoFilePath() {
                                return "123";
                            }
                        }
                );
    }

    @Test
    void whenFileNoExists() {
        assertRaw
                (
                        new Raw() {
                            @Override
                            public Metadata metadata() {
                                return new MetadataSkeleton() {
                                };
                            }

                            @Override
                            public String videoFilePath() {
                                return "/tmp/123.123";
                            }
                        }
                );
    }

    @Test
    void whenVideoIsSave() throws IOException {

        final String randomString = UUID.randomUUID().toString();

        final Path path = Path.of(randomString);

        Files.createFile(path);

        final AtomicReference<Raw> atomicReference = new AtomicReference<>();

        final Metadata metadataSkeleton = new MetadataSkeleton();

        new StorageProxy
                (
                        atomicReference::set,
                        null
                )
                .write
                        (
                                new Raw() {

                                    @Override
                                    public Metadata metadata() {
                                        return metadataSkeleton;
                                    }

                                    @Override
                                    public String videoFilePath() {
                                        return randomString;
                                    }
                                }
                        );

        final Raw raw = atomicReference.get();

        Assertions.assertNotNull(raw);

        Assertions.assertEquals(randomString, raw.videoFilePath());

        final Metadata metadata = raw.metadata();

        Assertions.assertNotNull(metadata);

        Assertions.assertEquals(metadataSkeleton.author(), metadata.author());

        Assertions.assertEquals(metadataSkeleton.tags(), metadata.tags());

        Assertions.assertEquals(metadataSkeleton.name(), metadata.name());

        Assertions.assertEquals(metadataSkeleton.createDate(), metadata.createDate());

        Files.delete(path);

    }

    private static void assertRaw(final Raw raw) {

        final AtomicReference<Message> atomicReference = new AtomicReference<>();

        final Storage storage = new StorageProxy(new StorageNullObject(), atomicReference::set);

        storage.write(raw);

        new InformationAssert("storage", "parameters invalid")
                .validate
                        (
                                Event.VIDEO_EMPTY, atomicReference.get()
                        );
    }

}