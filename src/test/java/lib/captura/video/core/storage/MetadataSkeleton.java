package lib.captura.video.core.storage;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

class MetadataSkeleton implements Metadata {

    MetadataSkeleton() {
        value = "base";
    }

    private final String value;

    @Override
    public String name() {
        return value;
    }

    @Override
    public String author() {
        return value;
    }

    @Override
    public Collection<String> tags() {
        return List.of(value);
    }

    @Override
    public Optional<LocalDate> createDate() {
        return Optional.of(LocalDate.now());
    }

}
