package lib.captura.video.core.repository;

import lib.captura.video.core.InformationAssert;
import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.Message;
import lib.captura.video.core.source.VideoSource;
import lib.captura.video.core.source.VideoSourceEntryNullObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

class FactoryNullObjectTest {

    @Test
    void shouldReturnFactoryInstance() {

        final AtomicReference<Message> atomicReference = new AtomicReference<>();

        final VideoSource videoSource = new FactoryNullObject().build(atomicReference::set);

        Assertions.assertInstanceOf(VideoSourceEntryNullObject.class, videoSource);

        new InformationAssert("Factory", "Video Source Not Found")
                .validate(Event.VIDEO_SOURCE_NOT_FOUND, atomicReference.get());

    }
}