package lib.captura.video.core.repository;

import lib.captura.video.core.InformationAssert;
import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.Message;
import lib.captura.video.core.source.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;


class RepositoryTest {

    @Test
    void shouldRegisterFactory() {

        final AtomicReference<Message> channel = new AtomicReference<>();

        final VideoSource videoSource =
                Repository
                        .create()
                        .registerFactory
                                (
                                        new Factory<>() {
                                            @Override
                                            public VideoSourceEntry build(Channel channel) {
                                                return new VideoSourceEntry() {
                                                    @Override
                                                    public Collection<String> requiredParameters() {
                                                        return List.of("param");
                                                    }

                                                    @Override
                                                    public Video video(final Map<String, String> parameters) {
                                                        throw new UnsupportedOperationException();
                                                    }
                                                };
                                            }

                                            @Override
                                            public String name() {
                                                return "1";
                                            }
                                        }
                                )
                        .videoSource("1", channel::set);

        final Map<String, String> parameters = Map.of("param", "value");

        Assertions.assertThrows
                (
                        UnsupportedOperationException.class,
                        () -> videoSource.video(parameters)
                );

        new InformationAssert("param", parameters.get("param"))
                .validate(Event.VIDEO_SOURCE_FOUND, channel.get());

    }

    @Test
    void shouldSendMessageToChannel() {

        final AtomicReference<Message> atomicReference = new AtomicReference<>();

        final VideoSource videoSource =
                Repository
                        .create()
                        .registerFactory
                                (
                                        new Factory<>() {
                                            @Override
                                            public VideoSourceEntry build(final Channel channel) {
                                                return new VideoSourceEntry() {
                                                    @Override
                                                    public Collection<String> requiredParameters() {
                                                        return List.of("param");
                                                    }

                                                    @Override
                                                    public Video video(final Map<String, String> parameters) {
                                                        throw new UnsupportedOperationException();
                                                    }
                                                };
                                            }

                                            @Override
                                            public String name() {
                                                return "1";
                                            }
                                        }
                                )
                        .videoSource("1", atomicReference::set);

        Assertions.assertInstanceOf(VideoSourceProxy.class, videoSource);

        final Video video = videoSource.video(Map.of("param1", "value"));

        Assertions.assertInstanceOf(VideoNullObject.class, video);

        new InformationAssert("parameters", "param")
                .validate(Event.PARAMETERS_NOT_PROVIDED, atomicReference.get());

    }
}