package lib.captura.video.core;

import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.Message;
import lib.captura.video.core.channel.Information;
import lib.captura.video.core.channel.InformationBasic;
import org.junit.jupiter.api.Assertions;

import java.util.Collection;
import java.util.Optional;

public final class InformationAssert extends InformationBasic {

    public InformationAssert(final String keyObject, final String valueObject) {
        super(keyObject, valueObject);
    }

    public void validate
            (
                    final Event event,
                    final Message message
            ) {

        Assertions.assertNotNull(message);

        Assertions.assertEquals(event, message.event());

        final Collection<Information> collection = message.infos();

        final Optional<Information> optional = collection.stream().findFirst();

        Assertions.assertTrue(optional.isPresent());

        final Information information = optional.get();

        Assertions.assertEquals(key(), information.key());

        Assertions.assertEquals(value(), information.value());
    }

}
