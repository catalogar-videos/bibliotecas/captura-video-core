package lib.captura.video.core.source;

import lib.captura.video.core.storage.StorageNullObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class VideoNullObjectTest {

    @Test
    void shouldDoNothing() {
        final VideoNullObject videoObject = new VideoNullObject();
        final StorageNullObject storage = new StorageNullObject();
        Assertions.assertDoesNotThrow(() -> videoObject.write(storage));
    }

}