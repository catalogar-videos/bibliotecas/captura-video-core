package lib.captura.video.core.source;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;


class VideoSourceEntryNullObjectTest {

    @Test
    void shouldReturnEmptyCollection() {
        Assertions.assertTrue(new VideoSourceEntryNullObject().requiredParameters().isEmpty());
    }

    @Test
    void shouldReturnVideoInstance() {
        Assertions.assertNotNull(new VideoSourceEntryNullObject().video(Map.of()));
    }
    
}