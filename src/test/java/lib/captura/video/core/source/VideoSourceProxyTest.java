package lib.captura.video.core.source;

import lib.captura.video.core.InformationAssert;
import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.Information;
import lib.captura.video.core.channel.Message;
import lib.captura.video.core.repository.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

class VideoSourceProxyTest {

    @ValueSource(strings = {"param1", "param1,param3", ""})
    @ParameterizedTest
    void whenRequiredParametersAreNotProvider(final String entry) {

        final Map<String, String> map = Arrays
                .stream(entry.split(","))
                .collect(Collectors.toMap(key -> key, key -> key));

        final AtomicReference<Message> atomicReference = new AtomicReference<>();

        final VideoSourceProxy videoSourceProxy = new VideoSourceProxy
                (
                        new Factory<>() {
                            @Override
                            public VideoSourceEntry build(Channel channel) {
                                return new VideoSourceEntry() {
                                    @Override
                                    public Collection<String> requiredParameters() {
                                        return List.of("param1", "param2");
                                    }

                                    @Override
                                    public Video video(final Map<String, String> parameters) {
                                        throw new IllegalCallerException();
                                    }
                                };
                            }

                            @Override
                            public String name() {
                                return "1";
                            }

                        },
                        atomicReference::set
                );

        final Video video = videoSourceProxy.video(map);

        Assertions.assertInstanceOf(VideoNullObject.class, video);

        new InformationAssert("parameters", "param1,param2")
                .validate
                        (
                                Event.PARAMETERS_NOT_PROVIDED,
                                atomicReference.get()
                        );

    }

    @ValueSource(strings = {"param1,param2", "param1,param3,param2"})
    @ParameterizedTest
    void whenRequiredParametersAreProvider(final String entry) {

        final Map<String, String> parameters = Arrays
                .stream(entry.split(","))
                .collect(Collectors.toMap(key -> key, key -> key));

        final AtomicReference<Message> channel = new AtomicReference<>();

        final VideoSourceProxy videoSourceProxy = new VideoSourceProxy
                (
                        new Factory<>() {
                            @Override
                            public VideoSourceEntry build(Channel channel) {
                                return new VideoSourceEntry() {
                                    @Override
                                    public Collection<String> requiredParameters() {
                                        return List.of("param1", "param2");
                                    }

                                    @Override
                                    public Video video(final Map<String, String> parameters) {
                                        return new VideoNullObject();
                                    }
                                };
                            }

                            @Override
                            public String name() {
                                return "1";
                            }
                        },
                        channel::set
                );

        Assertions.assertNotNull(videoSourceProxy.video(parameters));

        Assertions.assertFalse(parameters.isEmpty());

        final Message message = channel.get();

        Assertions.assertNotNull(message);

        Assertions.assertEquals(Event.VIDEO_SOURCE_FOUND, message.event());

        final Collection<Information> infos = message.infos();

        Assertions.assertFalse(infos.isEmpty());

        Assertions.assertTrue
                (
                        infos.stream()
                                .allMatch
                                        (
                                                i ->
                                                        parameters.containsKey(i.key()) &&
                                                                parameters.containsValue(i.value())
                                        )
                );

    }

}