package lib.captura.video.core.channel;

import lib.captura.video.core.InformationAssert;
import org.junit.jupiter.api.Test;

import java.util.List;

class MessageBuilderTest {

    @Test
    void shouldBuildMessage() {

        final Event parametersNotProvided = Event.PARAMETERS_NOT_PROVIDED;

        final InformationAssert information = new InformationAssert("1", "2");

        final Message message = MessageBuilder.builder()
                .addInformation(information)
                .withEvent(parametersNotProvided)
                .build();

        information.validate(parametersNotProvided, message);

    }

    @Test
    void shouldBuildMessageWithAllInformation() {

        final Event parametersNotProvided = Event.VIDEO_EMPTY;

        final InformationAssert information = new InformationAssert("1", "2");

        final Message message = MessageBuilder.builder()
                .addAllInformation(List.of(information))
                .withEvent(parametersNotProvided)
                .build();

        information.validate(parametersNotProvided, message);

    }

}