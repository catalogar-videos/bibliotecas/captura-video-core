package lib.captura.video.core.activity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

class ActivityTest {

    @Test
    void shouldReturnValue() {
        Assertions.assertTrue(Activity.entry(true).then());
    }

    @Test
    void shouldReturnValueMapped() {
        Assertions.assertEquals(1f, Activity.entry(1L).action(Long::floatValue).then());
    }

    @Test
    void shouldConsumeAMessage() {

        final AtomicInteger message = new AtomicInteger();

        Activity.entry(1).call(message::set).then();

        Assertions.assertEquals(1, message.get());

    }

    @Test
    void shouldSendMessageCommand() {

        final AtomicInteger message = new AtomicInteger();

        final int integer = Activity.entry(1).command(() -> message.set(2)).then();

        Assertions.assertEquals(2, message.get());

        Assertions.assertEquals(1, integer);

    }

    @Test
    void whenGuardIsTrueShouldReturnEntry() {

        final Optional<Integer> optional = Activity.entry(1)
                .guard(integer -> integer > 0)
                .and(integer -> integer > -1)
                .or(integer -> integer == 1)
                .transition()
                .then();

        Assertions.assertTrue(optional.isPresent());

        Assertions.assertEquals(1, optional.get());

    }

    @Test
    void whenGuardIsFalseShouldReturnNullValue() {

        final Optional<Integer> optional = Activity.entry(1)
                .guard(integer -> integer > 1)
                .transition()
                .then();

        Assertions.assertTrue(optional.isEmpty());

    }

    @Test
    void whenGuardIsTrueShouldNotReturnAlternativeValue() {

        final Optional<Integer> optional = Activity.entry(1)
                .guard(integer -> integer > 0)
                .transition()
                .action(integer -> integer + 9)
                .then();

        Assertions.assertTrue(optional.isPresent());

        Assertions.assertEquals(10, optional.get());

    }

    @Test
    void whenGuardIsFalseShouldReturnAlternativeValue() {

        final int value = Activity.entry(1)
                .guard(integer -> integer > 1)
                .transition()
                .then()
                .orElse(10);

        Assertions.assertEquals(10, value);

    }

    @Test
    void whenGuardIsFalseShouldCallCommand() {

        final AtomicBoolean flag = new AtomicBoolean(false);

        final Optional<String> optional = Activity
                .entry("1")
                .guard(String::isBlank)
                .transition()
                .call(() -> {
                    throw new IllegalCallerException();
                })
                .orCall(() -> flag.set(true))
                .then();

        Assertions.assertTrue(optional.isEmpty());

        Assertions.assertTrue(flag.get());

    }

    @Test
    void whenGuardIsTrueShouldCallCommand() {

        final AtomicBoolean flag = new AtomicBoolean(false);

        final Optional<String> optional = Activity
                .entry("1")
                .guard(Predicate.not(String::isBlank))
                .transition()
                .call(() -> flag.set(true))
                .orCall(() -> {
                    throw new IllegalCallerException();
                })
                .then();

        Assertions.assertTrue(optional.isPresent());

        Assertions.assertTrue(flag.get());

    }

    @Test
    void whenGuardIsFalseShouldSendMessageCommand() {

        final AtomicBoolean flag = new AtomicBoolean(false);

        final Optional<Boolean> optional = Activity
                .entry(false)
                .guard(Boolean::booleanValue)
                .transition()
                .orCall(flag::set)
                .call(() -> {
                    throw new IllegalCallerException();
                })
                .then();

        Assertions.assertTrue(optional.isEmpty());

        Assertions.assertFalse(flag.get());

    }

    @Test
    void whenGuardIsTrueShouldSendMessageCommand() {

        final AtomicBoolean flag = new AtomicBoolean(false);

        final Optional<Boolean> optional = Activity
                .entry(true)
                .guard(Boolean::booleanValue)
                .transition()
                .call(flag::set)
                .orCall(_ -> {
                    throw new IllegalCallerException();
                })
                .then();

        Assertions.assertTrue(optional.isPresent());

        Assertions.assertTrue(flag.get());

    }

    @Test
    void whenGuardIsTrueTheCommandIsCalled() {

        final AtomicInteger counter = new AtomicInteger(0);

        float value = Activity
                .entry(1)
                .guard(v -> v > 0)
                .transition()
                .orCall(() -> counter.set(-10))
                .orCall(_ -> counter.accumulateAndGet(-10, Integer::sum))
                .action(integer -> integer)
                .command(counter::incrementAndGet)
                .call(counter::addAndGet)
                .action(Integer::floatValue)
                .command(counter::incrementAndGet)
                .call(f -> counter.addAndGet(f.intValue()))
                .then()
                .orElseThrow();

        Assertions.assertEquals(4, counter.get());

        Assertions.assertEquals(1f, value);

    }

    @Test
    void whenGuardIsFalseTheCommandIsNotCalled() {

        final AtomicInteger counter = new AtomicInteger(0);

        final Optional<Float> optional =
                Activity.entry(1)
                        .guard(v -> v > 1)
                        .transition()
                        .call(() -> counter.set(-10))
                        .call(_ -> counter.accumulateAndGet(-10, Integer::sum))
                        .action(integer -> integer)
                        .command(counter::incrementAndGet)
                        .call(counter::addAndGet)
                        .action(Integer::floatValue)
                        .command(counter::incrementAndGet)
                        .call(f -> counter.addAndGet(f.intValue()))
                        .then();

        Assertions.assertThrows
                (
                        IllegalCallerException.class,
                        () -> optional.orElseThrow(IllegalCallerException::new)
                );
    }

}