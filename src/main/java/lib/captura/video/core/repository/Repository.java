package lib.captura.video.core.repository;

import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.source.VideoSource;
import lib.captura.video.core.source.VideoSourceEntry;


/**
 * The Repository interface represents a repository of video sources.
 */
public sealed interface Repository permits Repository.RepositoryRegister, RepositoryImpl {

    /**
     * Creates a new instance of the RepositoryRegister interface.
     *
     * @return A new instance of the RepositoryRegister interface.
     */
    static RepositoryRegister create() {
        return new RepositoryImpl();
    }

    /**
     * Retrieves a video source based on the video source name and channel.
     *
     * @param videoSourceName the name of the video source
     * @param channel         the channel used for video communication
     * @return the video source
     */
    VideoSource videoSource(final String videoSourceName, final Channel channel);

    /**
     * The RepositoryRegister interface represents a register for video source factories in the repository.
     */
    sealed interface RepositoryRegister extends Repository permits RepositoryImpl {
        /**
         * Registers a video source factory in the repository.
         *
         * @param factory         The factory object used to create the video source entry.
         * @return The repository register object with the factory registered.
         */
        RepositoryRegister registerFactory(final Factory<? extends VideoSourceEntry> factory);
    }

}
