package lib.captura.video.core.repository;

import lib.captura.video.core.channel.Channel;

/**
 * The Factory interface represents a factory that builds an object of type T.
 *
 * @param <T> the type of object that the factory builds
 */
public interface Factory<T> {

    /**
     * Builds an object of type T using the given channel.
     *
     * @param channel the channel used for building the object
     * @return the built object
     */
    T build(final Channel channel);

    /**
     * Returns the name of the factory.
     *
     * @return The name of the factory
     */
    String name();

}
