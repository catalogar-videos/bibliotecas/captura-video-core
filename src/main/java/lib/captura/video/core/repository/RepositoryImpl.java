package lib.captura.video.core.repository;

import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.source.VideoSource;
import lib.captura.video.core.source.VideoSourceEntry;
import lib.captura.video.core.source.VideoSourceProxy;

import java.util.HashMap;
import java.util.Map;

final class RepositoryImpl implements Repository.RepositoryRegister, Repository {

    RepositoryImpl() {
        factories = new HashMap<>();
    }

    private final Map<String, Factory<VideoSource>> factories;

    @Override
    public VideoSource videoSource(final String videoSourceName, final Channel channel) {
        return factories.getOrDefault(videoSourceName, new FactoryNullObject()).build(channel);
    }

    @Override
    public RepositoryRegister registerFactory(final Factory<? extends VideoSourceEntry> factory) {

        factories.put
                (
                        factory.name(),

                        new Factory<>() {
                            @Override
                            public VideoSource build(final Channel channel) {
                                return new VideoSourceProxy(factory, channel);
                            }

                            @Override
                            public String name() {
                                return factory.name();
                            }

                        }
                );

        return this;

    }

}
