package lib.captura.video.core.repository;

import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.MessageBuilder;
import lib.captura.video.core.channel.InformationBasic;
import lib.captura.video.core.source.VideoSource;
import lib.captura.video.core.source.VideoSourceEntryNullObject;

public final class FactoryNullObject implements Factory<VideoSource> {

    @Override
    public VideoSource build(final Channel channel) {

        channel.send
                (
                        MessageBuilder
                                .builder()
                                .withEvent(Event.VIDEO_SOURCE_NOT_FOUND)
                                .addInformation
                                        (
                                                new InformationBasic
                                                        (
                                                                "Factory",
                                                                "Video Source Not Found"
                                                        )
                                        )
                                .build()
                );

        return new VideoSourceEntryNullObject();

    }

    @Override
    public String name() {
        return "FactoryNullObject";
    }

}
