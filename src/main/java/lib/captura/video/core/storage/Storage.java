package lib.captura.video.core.storage;

/**
 * The Storage interface represents a storage system for storing resumes.
 */
public interface Storage {

    /**
     * Writes the raw data to the storage.
     *
     * @param raw The raw data to be written
     */
    void write(final Raw raw);

}
