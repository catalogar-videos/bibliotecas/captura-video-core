package lib.captura.video.core.storage;

import lib.captura.video.core.activity.Activity;
import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.InformationBasic;
import lib.captura.video.core.channel.MessageBuilder;

import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

public final class StorageProxy implements Storage {

    public StorageProxy(final Storage storage, final Channel channel) {
        this.storage = new StoragePredicateProxy(storage, channel);
    }

    private final Storage storage;

    @Override
    public void write(final Raw raw) {

        final Metadata metadataInfo = raw.metadata();

        final String pathLocalFile = Optional.ofNullable(raw.videoFilePath()).orElse("");

        storage.write
                (
                        new Raw() {

                            @Override
                            public Metadata metadata() {

                                return new Metadata() {

                                    @Override
                                    public String name() {
                                        return Optional.ofNullable(metadataInfo.name()).orElse("");
                                    }

                                    @Override
                                    public String author() {
                                        return Optional.ofNullable(metadataInfo.author()).orElse("");
                                    }

                                    @Override
                                    public Collection<String> tags() {
                                        return metadataInfo.tags();
                                    }

                                    @Override
                                    public Optional<LocalDate> createDate() {
                                        return metadataInfo.createDate();
                                    }

                                };

                            }

                            @Override
                            public String videoFilePath() {
                                return pathLocalFile;
                            }

                        }
                );

    }

    private static final class StoragePredicateProxy implements Storage {

        private StoragePredicateProxy(final Storage storage, final Channel channel) {
            this.storage = storage;
            this.channel = channel;
        }

        private final Channel channel;

        private final Storage storage;

        @Override
        public void write(final Raw raw) {

            Activity
                    .entry(raw)
                    .guard(Objects::isNull)
                    .or(r -> r.videoFilePath().isBlank())
                    .or(r -> r.metadata().name().isBlank())
                    .or(r -> r.metadata().author().isBlank())
                    .or(Predicate.not(r -> Files.exists(Path.of(r.videoFilePath()))))
                    .transition()
                    .call(() ->
                            channel.send
                                    (
                                            MessageBuilder
                                                    .builder()
                                                    .withEvent(Event.VIDEO_EMPTY)
                                                    .addInformation
                                                            (
                                                                    new InformationBasic
                                                                            (
                                                                                    "storage",
                                                                                    "parameters invalid"
                                                                            )
                                                            )
                                                    .build()
                                    )

                    )
                    .call(new StorageNullObject()::write)
                    .orCall(storage::write);

        }
    }

}
