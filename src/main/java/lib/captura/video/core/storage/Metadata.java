package lib.captura.video.core.storage;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

/**
 * The Metadata interface represents metadata information about an object.
 * It provides methods to retrieve the name, author, tags, and create date of the object.
 */
public interface Metadata {

    String name();

    String author();

    Collection<String> tags();

    Optional<LocalDate> createDate();

}
