package lib.captura.video.core.storage;


/**
 * The Raw interface represents raw data used for storage.
 */
public interface Raw {

    Metadata metadata();

    String videoFilePath();

}
