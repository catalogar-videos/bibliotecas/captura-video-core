package lib.captura.video.core.activity;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

final class ActivityGuardFalse<E> implements Activity.ActivityGuard<E> {

    ActivityGuardFalse(final E e) {
        this.e = e;
    }

    private final E e;

    @Override
    public <M> Activity.ActivityGuardInit<M> action(final Function<? super E, ? extends M> action) {
        return new MyActivityGuardInitFalse<>();
    }

    @Override
    public Activity.ActivityGuard<E> call(final Consumer<? super E> consumer) {
        return this;
    }

    @Override
    public Activity.ActivityGuard<E> orCall(final Consumer<? super E> consumer) {
        consumer.accept(e);
        return this;
    }

    @Override
    public Activity.ActivityGuard<E> call(final Runnable runnable) {
        return this;
    }

    @Override
    public Activity.ActivityGuard<E> orCall(final Runnable runnable) {
        runnable.run();
        return this;
    }

    @Override
    public Optional<E> then() {
        return Optional.empty();
    }

    private static final class MyActivityGuardInitFalse<M> implements Activity.ActivityGuardInit<M> {

        @Override
        public <M1> Activity.ActivityGuardInit<M1> action(final Function<? super M, ? extends M1> action) {
            return new MyActivityGuardInitFalse<>();
        }

        @Override
        public Activity.ActivityGuardInit<M> call(final Consumer<? super M> consumer) {
            return this;
        }

        @Override
        public Activity.ActivityGuardInit<M> command(final Runnable runnable) {
            return this;
        }

        @Override
        public Optional<M> then() {
            return Optional.empty();
        }

    }
}
