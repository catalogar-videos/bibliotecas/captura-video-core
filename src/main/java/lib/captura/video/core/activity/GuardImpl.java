package lib.captura.video.core.activity;

import java.util.function.Predicate;

final class GuardImpl<E> implements Activity.Guard<E> {

    GuardImpl(final E e, final Predicate<E> predicate) {
        this.e = e;
        this.predicate = predicate;
    }

    private final Predicate<E> predicate;

    private final E e;

    @Override
    public Activity.Guard<E> and(final Predicate<? super E> p) {
        return new GuardImpl<>(e, predicate.and(p));
    }

    @Override
    public Activity.Guard<E> or(final Predicate<? super E> p) {
        return new GuardImpl<>(e, predicate.or(p));
    }

    @Override
    public Activity.ActivityGuard<E> transition() {
        return predicate.test(e) ? new ActivityGuardTrue<>(e) : new ActivityGuardFalse<>(e);
    }

}
