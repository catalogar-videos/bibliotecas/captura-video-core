package lib.captura.video.core.activity;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

final class ActivityGuardTrue<E> implements Activity.ActivityGuard<E> {

    ActivityGuardTrue(final E e) {
        this.e = e;
    }

    private final E e;

    @Override
    public <M> Activity.ActivityGuardInit<M> action(final Function<? super E, ? extends M> action) {
        return new MyActivityGuardInitTrue<>(action.apply(e));
    }

    @Override
    public Activity.ActivityGuard<E> call(final Consumer<? super E> consumer) {
        consumer.accept(e);
        return this;
    }

    @Override
    public Activity.ActivityGuard<E> orCall(final Consumer<? super E> consumer) {
        return this;
    }

    @Override
    public Activity.ActivityGuard<E> call(final Runnable runnable) {
        runnable.run();
        return this;
    }

    @Override
    public Activity.ActivityGuard<E> orCall(final Runnable runnable) {
        return this;
    }

    @Override
    public Optional<E> then() {
        return Optional.of(e);
    }

    private static final class MyActivityGuardInitTrue<E> implements Activity.ActivityGuardInit<E> {

        private MyActivityGuardInitTrue(final E e) {
            this.e = e;
        }

        private final E e;

        @Override
        public <M> Activity.ActivityGuardInit<M> action(final Function<? super E, ? extends M> action) {
            return new MyActivityGuardInitTrue<>(action.apply(e));
        }

        @Override
        public Activity.ActivityGuardInit<E> call(final Consumer<? super E> consumer) {
            consumer.accept(e);
            return this;
        }

        @Override
        public Activity.ActivityGuardInit<E> command(final Runnable runnable) {
            runnable.run();
            return this;
        }

        @Override
        public Optional<E> then() {
            return Optional.ofNullable(e);
        }
    }
}
