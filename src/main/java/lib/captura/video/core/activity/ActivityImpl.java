package lib.captura.video.core.activity;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

final class ActivityImpl<E> implements Activity.ActivityInit<E> {

    ActivityImpl(final E e) {
        this.e = e;
    }

    private final E e;

    @Override
    public <M> ActivityInit<M> action(final Function<? super E, ? extends M> action) {
        return new ActivityImpl<>(action.apply(e));
    }

    @Override
    public ActivityInit<E> call(final Consumer<? super E> consumer) {
        consumer.accept(e);
        return this;
    }

    @Override
    public ActivityInit<E> command(final Runnable runnable) {
        runnable.run();
        return this;
    }

    @Override
    public E then() {
        return e;
    }

    @Override
    public Guard<E> guard(final Predicate<E> predicate) {
        return new GuardImpl<>(e, predicate);
    }

}

