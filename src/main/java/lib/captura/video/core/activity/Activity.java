package lib.captura.video.core.activity;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public interface Activity<E> {

    static <E> ActivityInit<E> entry(final E e) {
        return new ActivityImpl<>(e);
    }

    Guard<E> guard(final Predicate<E> predicate);

    sealed interface ActivityGuard<E> permits ActivityGuardFalse, ActivityGuardTrue {

        <M> ActivityGuardInit<M> action(final Function<? super E, ? extends M> action);

        ActivityGuard<E> call(final Consumer<? super E> consumer);

        ActivityGuard<E> orCall(final Consumer<? super E> consumer);

        ActivityGuard<E> call(final Runnable runnable);

        ActivityGuard<E> orCall(final Runnable runnable);

        Optional<E> then();

    }

    sealed interface Guard<E> permits GuardImpl {

        Guard<E> and(final Predicate<? super E> predicate);

        Guard<E> or(final Predicate<? super E> predicate);

        ActivityGuard<E> transition();

    }

    sealed interface ActivityInit<E> extends Activity<E> permits ActivityImpl {

        <M> ActivityInit<M> action(final Function<? super E, ? extends M> action);

        ActivityInit<E> call(final Consumer<? super E> consumer);

        ActivityInit<E> command(final Runnable runnable);

        E then();

    }

    interface ActivityGuardInit<E> {

        <M> ActivityGuardInit<M> action(final Function<? super E, ? extends M> action);

        ActivityGuardInit<E> call(final Consumer<? super E> consumer);

        ActivityGuardInit<E> command(final Runnable runnable);

        Optional<E> then();

    }
}
