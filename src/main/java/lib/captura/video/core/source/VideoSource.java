package lib.captura.video.core.source;

import java.util.Map;

/**
 * The VideoSource interface represents a video source.
 */
public interface VideoSource {

    /**
     * Returns a Video based on the given parameters.
     *
     * @param parameters a map of parameters required to retrieve the video
     * @return the Video object or an empty Optional if the parameters are not provided or invalid
     */
    Video video(final Map<String, String> parameters);

}
