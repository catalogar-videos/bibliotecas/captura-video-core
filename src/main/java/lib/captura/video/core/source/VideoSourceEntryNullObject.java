package lib.captura.video.core.source;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public final class VideoSourceEntryNullObject implements VideoSourceEntry {

    @Override
    public Collection<String> requiredParameters() {
        return List.of();
    }

    @Override
    public Video video(final Map<String, String> parameters) {
        return new VideoNullObject();
    }

}
