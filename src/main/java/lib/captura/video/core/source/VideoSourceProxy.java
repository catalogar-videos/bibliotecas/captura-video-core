package lib.captura.video.core.source;

import lib.captura.video.core.activity.Activity;
import lib.captura.video.core.channel.Channel;
import lib.captura.video.core.channel.Event;
import lib.captura.video.core.channel.InformationBasic;
import lib.captura.video.core.channel.MessageBuilder;
import lib.captura.video.core.repository.Factory;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class VideoSourceProxy implements VideoSource {

    public VideoSourceProxy(final Factory<? extends VideoSourceEntry> factory, final Channel channel) {
        this.videoSourceEntry = factory.build(channel);
        this.channel = channel;
    }

    private final VideoSourceEntry videoSourceEntry;

    private final Channel channel;

    @Override
    public Video video(final Map<String, String> parameters) {

        final Collection<String> requiredParameters = videoSourceEntry.requiredParameters();

        return Activity
                .entry(parameters)
                .call
                        (
                                p -> channel.send
                                        (
                                                MessageBuilder
                                                        .builder()
                                                        .withEvent(Event.VIDEO_SOURCE_FOUND)
                                                        .addAllInformation
                                                                (
                                                                        p.entrySet()
                                                                                .stream()
                                                                                .map(InformationBasic::new)
                                                                                .collect(Collectors.toSet())
                                                                )
                                                        .build()
                                        )
                        )
                .action(Map::keySet)
                .guard(e -> e.containsAll(requiredParameters))
                .transition()
                .orCall(() -> channel.send
                        (
                                MessageBuilder.builder()
                                        .withEvent(Event.PARAMETERS_NOT_PROVIDED)
                                        .addInformation
                                                (
                                                        new InformationBasic
                                                                (
                                                                        "parameters",
                                                                        String.join
                                                                                (
                                                                                        ",",
                                                                                        requiredParameters
                                                                                )
                                                                )
                                                )
                                        .build()
                        )
                )
                .action(_ -> videoSourceEntry.video(parameters))
                .then()
                .orElseGet(VideoNullObject::new);

    }

}

