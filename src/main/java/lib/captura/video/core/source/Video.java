package lib.captura.video.core.source;

import lib.captura.video.core.storage.Storage;

/**
 * The Video interface represents a video object.
 *
 * <p>
 * The Video interface includes one method that writes the video to a storage.
 * </p>
 */
public interface Video {

    /**
     * Writes the video to a storage.
     *
     * @param storage The storage where the
     */
    void write(final Storage storage);

}
