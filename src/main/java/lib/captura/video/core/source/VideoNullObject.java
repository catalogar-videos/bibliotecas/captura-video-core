package lib.captura.video.core.source;

import lib.captura.video.core.storage.Storage;

public final class VideoNullObject implements Video {

    @Override
    public void write(final Storage storage) {
        // Not Implemented
    }

}
