package lib.captura.video.core.source;

import java.util.Collection;

/**
 * The VideoSourceEntry interface represents a video source entry that implements the VideoSource interface.
 */
public interface VideoSourceEntry extends VideoSource {

    /**
     * Returns a collection of the required parameters for this video source entry.
     *
     * @return A collection of required parameters
     */
    Collection<String> requiredParameters();

}
