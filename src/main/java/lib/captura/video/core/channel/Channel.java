package lib.captura.video.core.channel;

/**
 * The Channel interface represents a communication channel to send messages.
 */
public interface Channel {

    /**
     * Sends a message through the channel.
     *
     * @param message The message to be sent
     */
    void send(final Message message);

}
