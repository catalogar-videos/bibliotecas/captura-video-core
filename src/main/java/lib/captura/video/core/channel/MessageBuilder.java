package lib.captura.video.core.channel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class MessageBuilder {

    public static MessageBuilder builder() {
        return new MessageBuilder();
    }

    private MessageBuilder() {
        infos = new ArrayList<>();
    }

    private final Collection<Information> infos;

    public MessageBuilder addInformation(final Information information) {
        infos.add(information);
        return this;
    }

    public MessageBuilder addAllInformation(final Collection<Information> infos) {
        this.infos.addAll(infos);
        return this;
    }

    private Event eventObject;

    public MessageBuilder withEvent(final Event eventObject) {
        this.eventObject = eventObject;
        return this;
    }

    public Message build() {

        return new Message() {
            @Override
            public Event event() {
                return eventObject;
            }

            @Override
            public Collection<Information> infos() {
                return List.copyOf(infos);
            }

        };

    }

}
