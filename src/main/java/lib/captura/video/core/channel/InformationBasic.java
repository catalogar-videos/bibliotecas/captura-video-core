package lib.captura.video.core.channel;

import java.util.Map;

public class InformationBasic implements Information {

    public InformationBasic(final String keyObject, final String valueObject) {
        this.keyObject = keyObject;
        this.valueObject = valueObject;
    }

    public InformationBasic(final Map.Entry<String, String> e) {
        keyObject = e.getKey();
        valueObject = e.getValue();
    }

    private final String keyObject;

    private final String valueObject;

    @Override
    public String key() {
        return keyObject;
    }

    @Override
    public String value() {
        return valueObject;
    }

}
