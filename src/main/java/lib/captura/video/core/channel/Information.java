package lib.captura.video.core.channel;

/**
 * The Information interface represents a key-value pair of information.
 */
public interface Information {

    String key();

    String value();

}
