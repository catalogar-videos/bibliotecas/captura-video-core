package lib.captura.video.core.channel;

import java.util.Collection;

/**
 * The Message interface represents a message object.
 */
public interface Message {

    Event event();

    Collection<Information> infos();

}
